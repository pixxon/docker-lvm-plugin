FROM alpine:3.19.1 as builder

RUN apk add go
WORKDIR /app
COPY src .
RUN go mod download
RUN go build -o volume-plugin ./cmd/volume-plugin

FROM alpine:3.19.1

WORKDIR /app
RUN apk add lvm2 e2fsprogs
COPY --from=builder /app/volume-plugin /app/volume-plugin
ENTRYPOINT ["/app/volume-plugin"]
