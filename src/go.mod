module gitlab.com/pixxon/docker-lvm-plugin

go 1.21.8

require (
	github.com/docker/go-plugins-helpers v0.0.0-20211224144127-6eecb7beb651
	github.com/docker/go-units v0.5.0
	github.com/filcloud/lvm v0.0.0-20200113075405-2d4f262786a4
)

require (
	github.com/Microsoft/go-winio v0.6.1 // indirect
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf // indirect
	github.com/docker/go-connections v0.5.0 // indirect
	github.com/gofrs/flock v0.8.1 // indirect
	golang.org/x/mod v0.17.0 // indirect
	golang.org/x/sys v0.19.0 // indirect
	golang.org/x/tools v0.19.0 // indirect
	gopkg.in/freddierice/go-losetup.v1 v1.0.0-20170407175016-fc9adea44124 // indirect
)
