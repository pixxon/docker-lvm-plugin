package main

import (
	"log/slog"
	"os"
	"os/user"
	"strconv"

	"github.com/docker/go-plugins-helpers/volume"
)

func main() {
	slog.Info("start")

	vgname, found := os.LookupEnv("VOLUME_GROUP")
	if !found || vgname == "" {
		slog.Error("volume group is not set via VOLUME_GROUP")
		return
	}

	d, err := NewDriver("/mnt/volumes", vgname)
	if err != nil {
		slog.Error("could not init driver", "error", err)
		return
	}

	u, err := user.Lookup("root")
	if err != nil {
		slog.Error("could not find root user", "error", err)
		return
	}

	gid, err := strconv.Atoi(u.Gid)
	if err != nil {
		slog.Error("could not convert group ID", "error", err)
		return
	}

	h := volume.NewHandler(d)
	h.ServeUnix("lvm", gid)
}
