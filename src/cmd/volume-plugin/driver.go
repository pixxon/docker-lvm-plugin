package main

import (
	"fmt"
	"log/slog"
	"path"
	"time"

	"github.com/docker/go-plugins-helpers/volume"
	"github.com/docker/go-units"
	"github.com/filcloud/lvm"
)

type lvmVolume struct {
	Name          string
	Mountpoint    string
	CreatedAt     time.Time
	Count         int
	LogicalVolume *lvm.LogicalVolume
}

type lvmVolumeDriver struct {
	Volumes     map[string]*lvmVolume
	VolumeGroup *lvm.VolumeGroup
	Mountroot   string
}

func NewDriver(mountroot string, vgname string) (*lvmVolumeDriver, error) {
	slog.Info("new driver", "vg", vgname, "root", mountroot)

	vg, err := lvm.LookupVolumeGroup(vgname)
	if err != nil {
		return nil, err
	}

	lvnames, err := vg.ListLogicalVolumeNames()
	if err != nil {
		return nil, err
	}

	vols := make(map[string]*lvmVolume)
	for _, lvname := range lvnames {
		slog.Info("found logical volume", "lv", lvname)

		lv, err := vg.LookupLogicalVolume(lvname)
		if err != nil {
			slog.Error("skipping logical volume", "error", err)
			continue
		}

		vol := &lvmVolume{
			Name:          lvname,
			Mountpoint:    path.Join(mountroot, lvname),
			CreatedAt:     time.Now(),
			Count:         0,
			LogicalVolume: lv,
		}

		vols[lvname] = vol
	}

	d := &lvmVolumeDriver{
		Volumes:     vols,
		VolumeGroup: vg,
		Mountroot:   mountroot,
	}

	return d, nil
}

func (d lvmVolumeDriver) Create(r *volume.CreateRequest) error {
	slog.Info("create", "name", r.Name)

	if _, exists := d.Volumes[r.Name]; exists {
		return fmt.Errorf("volume already exists with requested Name")
	}

	if lv, err := d.VolumeGroup.LookupLogicalVolume(r.Name); lv != nil {
		return err
	}

	sizeInBytes, err := units.FromHumanSize(r.Options["size"])
	if err != nil {
		return err
	}
	sizeInBytes = (sizeInBytes + 511) / 512 * 512

	lv, err := d.VolumeGroup.CreateLogicalVolume(r.Name, uint64(sizeInBytes), nil)
	if err != nil {
		return err
	}

	lvpath, err := lv.Path()
	if err != nil {
		return err
	}

	err = Format(lvpath)
	if err != nil {
		return err
	}

	d.Volumes[r.Name] = &lvmVolume{
		Name:          r.Name,
		Mountpoint:    path.Join(d.Mountroot, r.Name),
		CreatedAt:     time.Now(),
		Count:         0,
		LogicalVolume: lv,
	}

	return nil
}

func (d lvmVolumeDriver) List() (*volume.ListResponse, error) {
	slog.Info("list")

	var vols []*volume.Volume
	for _, vol := range d.Volumes {
		v := &volume.Volume{
			Name:       vol.Name,
			Mountpoint: vol.Mountpoint,
			CreatedAt:  vol.CreatedAt.Format(time.RFC3339),
		}
		vols = append(vols, v)
	}
	return &volume.ListResponse{Volumes: vols}, nil
}

func (d lvmVolumeDriver) Get(r *volume.GetRequest) (*volume.GetResponse, error) {
	slog.Info("get", "name", r.Name)

	v, exists := d.Volumes[r.Name]
	if !exists {
		return nil, fmt.Errorf("volume does not exists with requested Name")
	}

	var vol = &volume.Volume{
		Name:       v.Name,
		Mountpoint: v.Mountpoint,
		CreatedAt:  v.CreatedAt.Format(time.RFC3339),
	}

	return &volume.GetResponse{Volume: vol}, nil
}

func (d lvmVolumeDriver) Remove(r *volume.RemoveRequest) error {
	slog.Info("remove", "name", r.Name)

	v, exists := d.Volumes[r.Name]
	if !exists {
		return fmt.Errorf("volume does not exists with requested Name")
	}

	err := v.LogicalVolume.Remove()
	if err != nil {
		return err
	}

	delete(d.Volumes, r.Name)

	return nil
}

func (d lvmVolumeDriver) Path(r *volume.PathRequest) (*volume.PathResponse, error) {
	slog.Info("path", "name", r.Name)

	v, exists := d.Volumes[r.Name]
	if !exists {
		return nil, fmt.Errorf("volume does not exists with requested Name")
	}

	return &volume.PathResponse{Mountpoint: v.Mountpoint}, nil
}

func (d lvmVolumeDriver) Mount(r *volume.MountRequest) (*volume.MountResponse, error) {
	slog.Info("mount", "name", r.Name, "container", r.ID)

	v, exists := d.Volumes[r.Name]
	if !exists {
		return nil, fmt.Errorf("volume does not exists with requested Name")
	}

	if v.Count == 0 {
		lvpath, err := v.LogicalVolume.Path()
		if err != nil {
			return nil, err
		}

		if err = Mount(lvpath, v.Mountpoint); err != nil {
			return nil, err
		}
	}

	v.Count++

	slog.Info("mounted volume", "name", v.Name, "count", v.Count, "container", r.ID)

	return &volume.MountResponse{Mountpoint: v.Mountpoint}, nil
}

func (d lvmVolumeDriver) Unmount(r *volume.UnmountRequest) error {
	slog.Info("unmount", "name", r.Name, "container", r.ID)

	v, exists := d.Volumes[r.Name]
	if !exists {
		return fmt.Errorf("volume does not exists with requested Name")
	}

	v.Count--

	if v.Count == 0 {
		err := Unmount(v.Mountpoint)
		if err != nil {
			return err
		}
	}

	slog.Info("unmounted volume", "name", v.Name, "count", v.Count, "container", r.ID)

	return nil
}

func (d lvmVolumeDriver) Capabilities() *volume.CapabilitiesResponse {
	slog.Info("capabilities")

	return &volume.CapabilitiesResponse{
		Capabilities: volume.Capability{
			Scope: "local",
		},
	}
}
