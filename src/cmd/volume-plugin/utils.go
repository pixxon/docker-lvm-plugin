package main

import (
	"log/slog"
	"os"
	"os/exec"

	"github.com/filcloud/lvm"
)

type FakeLogger struct {
}

func (FakeLogger) Print(v ...interface{}) {
}

func (FakeLogger) Printf(format string, v ...interface{}) {
}

func init() {
	lvm.SetLogger(FakeLogger{})
}

func Mount(device string, target string) error {
	slog.Info("mount", "target", target, "device", device)

	if err := os.MkdirAll(target, os.ModePerm); err != nil {
		return err
	}

	cmd := exec.Command("mount", device, target)
	if out, err := cmd.CombinedOutput(); err != nil {
		slog.Error("mount failed", "output", out)
		return err
	}

	return nil
}

func Unmount(target string) error {
	slog.Info("unmount", "target", target)

	cmd := exec.Command("umount", target)
	if out, err := cmd.CombinedOutput(); err != nil {
		slog.Error("umount failed", "output", out)
		return err
	}

	if err := os.RemoveAll(target); err != nil {
		return err
	}

	return nil
}

func Format(device string) error {
	slog.Info("format", "device", device)

	cmd := exec.Command("mkfs.ext4", device)
	if out, err := cmd.CombinedOutput(); err != nil {
		slog.Error("mkfs.ext4 failed", "output", out)
		return err
	}

	return nil
}
